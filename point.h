#ifndef POINT_H
#define POINT_H

struct Point
{
    Point(int x, int y)
    {
        this->x = x;
        this->y = y;
    }
    int x;
    int y;
};

#endif // POINT_H
