#include "gamewindow.h"
#include "ui_gamewindow.h"
#include <QKeyEvent>
#include <QTimer>
#include <iostream>
#include <QMessageBox>

GameWindow::GameWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GameWindow)
{
    ui->setupUi(this);
    board = new Board();
    scene = new QGraphicsScene(ui->graphicsView);
    ui->graphicsView->setScene(scene);
    ui->graphicsView->setRenderHint(QPainter::Antialiasing);
    ui->numberPointsLabel->setText("0");
    scene->setSceneRect(board->boundingRect());
    scene->addItem(board);
    QTimer* timer = new QTimer(ui->graphicsView);
    QObject::connect(timer, SIGNAL(timeout()), scene, SLOT(advance()));
    QObject::connect(board, SIGNAL(scoreChanged(QString)), ui->numberPointsLabel, SLOT(setText(QString)));
    QObject::connect(board, SIGNAL(gameWon()), this, SLOT(gameWon()));
    timer->start(1000 / 33);
}

GameWindow::~GameWindow()
{
    delete ui;
}

void GameWindow::keyPressEvent(QKeyEvent* keyEvent)
{
    if(keyEvent->key() == Qt::Key_W || keyEvent->key() == Qt::Key_D || keyEvent->key() == Qt::Key_S || keyEvent->key() == Qt::Key_A)
    {
        this->board->moveTiles(keyEvent->key());
    }
    else if(keyEvent->key() == Qt::Key_Backspace)
    {
        this->board->undo();
    }
    else if(keyEvent->key() == Qt::Key_Escape)
    {
        this->newGame();
    }

}

void GameWindow::fitInView()
{
    ui->graphicsView->fitInView(board, Qt::KeepAspectRatio);
}

void GameWindow::resizeEvent(QResizeEvent * event)
{
    QMainWindow::resizeEvent(event);
    fitInView();
}

void GameWindow::on_undoButton_clicked()
{
    this->board->undo();
}

void GameWindow::newGame()
{
    this->scene->removeItem(board);
    board = new Board();
    this->scene->addItem(board);
    ui->numberPointsLabel->setText("0");;
    QObject::connect(board, SIGNAL(scoreChanged(QString)), ui->numberPointsLabel, SLOT(setText(QString)));
    QObject::connect(board, SIGNAL(gameWon()), this, SLOT(gameWon()));
}

void GameWindow::gameWon()
{
    this->ui->graphicsView->repaint();
    QMessageBox::information(ui->graphicsView, "Victory", "Congratulations! You won!");
    this->newGame();
}
