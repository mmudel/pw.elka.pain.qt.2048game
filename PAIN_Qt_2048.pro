#-------------------------------------------------
#
# Project created by QtCreator 2016-04-24T18:21:50
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PAIN_Qt_2048
TEMPLATE = app


SOURCES += main.cpp\
        gamewindow.cpp \
    tile.cpp \
    board.cpp

HEADERS  += gamewindow.h \
    point.h \
    tile.h \
    board.h \
    constants.h

FORMS    += gamewindow.ui
