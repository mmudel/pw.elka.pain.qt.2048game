#ifndef CONSTANTS_H
#define CONSTANTS_H

#define numbersBase 2
#define boardWidth 500
#define speedModifier 8
#define winningNumber 2048

#endif // CONSTANTS_H
