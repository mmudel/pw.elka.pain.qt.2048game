#include "tile.h"
#include <algorithm>
#include <iostream>
#include <math.h>
#include <board.h>

using namespace std;

Tile::Tile(int number, QGraphicsItem *parent) : QGraphicsItem(parent), number(number)
{
    moveDiff = this->parentItem()->boundingRect().width()/speedModifier;
    this->mapColor();
}

int Tile::move()
{
    int flag = 0;
    if(moveX != 0 || moveY !=0)
        flag = 1;
    if(moveX < 0 || moveY <0)
        this->moveBy(-(min(abs(this->moveX), this->moveDiff)), -(min(abs(this->moveY), this->moveDiff)));
    else
        this->moveBy(min(abs(this->moveX), this->moveDiff), min(abs(this->moveY), this->moveDiff));
    if(this->moveX < 0)
        this->moveX += min(abs(this->moveX), this->moveDiff);
    else
        this->moveX -= min(abs(this->moveX), this->moveDiff);
    if(this->moveY < 0)
        this->moveY += min(abs(this->moveY), this->moveDiff);
    else
        this->moveY -= min(abs(this->moveY), this->moveDiff);
    if(moveX == 0 && moveY == 0 && flag == 1)
    {
        return 1;
    }
    if(moveX != 0 || moveY != 0)
    {
        return 2;
    }
    return 0;
}

void Tile::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setPen(Qt::black);
    painter->setBrush(this->color);
    painter->drawRoundRect(boundingRect());
    painter->setFont(QFont("Calbria", 20));
    painter->drawText(this->boundingRect(), Qt::AlignCenter, QString::number(this->number));
}

QRectF Tile::boundingRect() const
{
    return QRectF(0, 0, this->parentItem()->boundingRect().width()/4, this->parentItem()->boundingRect().height()/4);
}

int Tile::increaseNumber()
{
    this->number = number*numbersBase;
    this->mapColor();
    if(this->number == winningNumber)
        return 1;
    return 0;
}

void Tile::mapColor()
{
    switch(this->number)
    {
    case 2:
        this->color = Qt::yellow;
        break;
    case 4:
        this->color = Qt::red;
        break;
    case 8:
        this->color = Qt::blue;
        break;
    case 16:
        this->color = Qt::green;
        break;
    case 32:
        this->color = Qt::darkGray;
        break;
    case 64:
        this->color = Qt::darkCyan;
        break;
    case 128:
        this->color = Qt::darkBlue;
        break;
    case 256:
        this->color = Qt::darkGreen;
        break;
    case 512:
        this->color = Qt::darkYellow;
        break;
    case 1024:
        this->color = Qt::cyan;
        break;
    case 2048:
        this->color = Qt::magenta;
        break;
    }
}

int Tile::getNumber()
{
    return this->number;
}
