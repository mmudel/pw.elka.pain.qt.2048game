#include "board.h"
#include "point.h"
#include <iostream>

Board::Board(QObject *parent) : QObject(parent)
{
    for(int i =0; i<4; i++)
    {
        for(int j=0; j<4; j++)
        {
            this->tiles[i][j] = nullptr;
            this->newTiles[i][j] = nullptr;
            this->oldTiles[i][j] = nullptr;
        }
    }
    this->score=0;
    this->oldScore=0;
    createTile();
}

void Board::createTile()
{
    std::vector<Point> freeSpace;
    for(int i =0; i<4; i++)
    {
        for(int j=0; j<4; j++)
        {
            if(this->tiles[i][j] == nullptr)
            {
                freeSpace.push_back(Point(i, j));
            }
        }
    }
    int whichToFill = qrand() % freeSpace.size();
    Point spaceToFill = freeSpace[whichToFill];
    int whichNumber = rand() % 8;
    int number = 0;
    if(whichNumber == 0)
        number = numbersBase*numbersBase;
    else
        number = numbersBase;
    this->tiles[spaceToFill.x][spaceToFill.y] = new Tile(number, this);
    this->tiles[spaceToFill.x][spaceToFill.y]->setPos(this->boundingRect().x() + this->boundingRect().width()/4*spaceToFill.x, this->boundingRect().y() + this->boundingRect().height()/4*spaceToFill.y);
}

QRectF Board::boundingRect() const
{
    return QRectF((-(0.5*boardWidth)), (-(0.5*boardWidth)), boardWidth, boardWidth);
}

void Board::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

}

void Board::advance(int phase)
{
    if(phase == 1)
    {
        int oneStillMoving = 0;
        int oneEnded = 0;
        for(int i=0; i<4; i++)
        {
            for(int j=0; j<4; j++)
            {
                if(this->tiles[i][j] != nullptr)
                {
                    int a = this->tiles[i][j]->move();
                    if(a==2)
                        oneStillMoving=1;
                    else if(a==1)
                        oneEnded=1;
                    //this->tiles[i][j]->update();
                }
            }
        }
        if(oneEnded == 1 && oneStillMoving == 0)
            this->endMove();
    }
}

void Board::moveTiles(int key)
{
    int count = 0;
    bool hasMoved = false;
    int k;
    if(moving == true)
        return;
    moving = true;
    oldScore = score;
    for(int i=0; i<4; i++)
    {
        for(int j=0; j<4; j++)
        {
            this->oldTiles[i][j] = nullptr;
            this->newTiles[i][j] = nullptr;
            this->oldTiles[i][j] = this->tiles[i][j];
            if(this->tiles[i][j] != nullptr)
                this->oldTilesNumber[i][j] = this->tiles[i][j]->getNumber();
        }
    }
    switch(key)
    {
    case Qt::Key_W:
        for(int i = 0; i<4; i++)
        {
            for(int j=0; j<4; j++)
            {
                if(this->tiles[j][i] != nullptr)
                {
                    for(k = i-1; k>=0; k--)
                    {
                        if(this->newTiles[j][k] == nullptr)
                        {
                            count++;
                        }
                        else
                        {
                            if(this->newTiles[j][k]->getNumber() == this->tiles[j][i]->getNumber())
                            {
                                this->joinTiles(Point(j, i), Point(j, k));
                                count++;
                            }
                            break;
                        }
                    }
                    if(this->tiles[j][i] == nullptr || this->tiles[j][i]->destroy == false)
                        this->newTiles[j][i-count] = this->tiles[j][i];
                    this->tiles[j][i]->moveY=(- this->boundingRect().width()/4)*count;
                }
                if(count > 0)
                    hasMoved = true;
                count=0;
            }
        }
        break;
    case Qt::Key_D:
        for(int i = 0; i<4; i++)
        {
            for(int j=3; j>=0; j--)
            {
                if(this->tiles[j][i] != nullptr)
                {
                    for(k = j+1; k<4; k++)
                    {
                        if(this->newTiles[k][i] == nullptr)
                        {
                            count++;
                        }
                        else
                        {
                            if(this->newTiles[k][i]->getNumber() == this->tiles[j][i]->getNumber())
                            {
                                this->joinTiles(Point(j, i), Point(k, i));
                                count++;
                            }
                            break;
                        }
                    }
                    if(this->tiles[j][i] == nullptr || this->tiles[j][i]->destroy == false)
                        this->newTiles[j+count][i] = this->tiles[j][i];
                    this->tiles[j][i]->moveX=(this->boundingRect().width()/4)*count;
                }
                if(count > 0)
                    hasMoved = true;
                count=0;
            }
        }
        break;
    case Qt::Key_S:
        for(int i = 3; i>=0; i--)
        {
            for(int j=0; j<4; j++)
            {
                if(this->tiles[j][i] != nullptr)
                {
                    for(k = i+1; k<4; k++)
                    {
                        if(this->newTiles[j][k] == nullptr)
                        {
                            count++;
                        }
                        else
                        {
                            if(this->newTiles[j][k]->getNumber() == this->tiles[j][i]->getNumber())
                            {
                                this->joinTiles(Point(j, i), Point(j, k));
                                count++;
                            }
                            break;
                        }
                    }
                    if(this->tiles[j][i] == nullptr || this->tiles[j][i]->destroy == false)
                        this->newTiles[j][i+count] = this->tiles[j][i];
                    this->tiles[j][i]->moveY=(this->boundingRect().width()/4)*count;
                }
                if(count > 0)
                    hasMoved = true;
                count=0;
            }
        }
        break;
    case Qt::Key_A:
        for(int i = 0; i<4; i++)
        {
            for(int j=0; j<4; j++)
            {
                if(this->tiles[j][i] != nullptr)
                {
                    for(k = j-1; k>=0; k--)
                    {
                        if(this->newTiles[k][i] == nullptr)
                        {
                            count++;
                        }
                        else
                        {
                            if(this->newTiles[k][i]->getNumber() == this->tiles[j][i]->getNumber())
                            {
                                this->tiles[j][i]->destroy = true;
                                this->joinTiles(Point(j, i), Point(k, i));
                                count++;
                            }
                            break;
                        }
                    }
                    if(this->tiles[j][i] == nullptr || this->tiles[j][i]->destroy == false)
                        this->newTiles[j-count][i] = this->tiles[j][i];
                    this->tiles[j][i]->moveX=(- this->boundingRect().width()/4)*count;
                }
                if(count > 0)
                    hasMoved = true;
                count=0;
            }
        }
        break;
    }
    if(hasMoved == false)
    {
        moving = false;
    }
}

void Board::joinTiles(Point from, Point to)
{
    this->tiles[from.x][from.y]->destroy = true;
    if(this->newTiles[to.x][to.y]->increaseNumber())
        this->isGameWon = true;
    this->score = score + (newTiles[to.x][to.y]->getNumber());
    this->scoreChanged(QString::number(score));
}

void Board::endMove()
{
    for(int i=0; i<4; i++)
    {
        for(int j=0; j<4; j++)
        {
            if(this->tiles[i][j] != nullptr && this->tiles[i][j]->destroy == true)
                this->tiles[i][j]->deleteLater();
            this->tiles[i][j] = this->newTiles[i][j];
            this->newTiles[i][j] = nullptr;
        }
    }
    createTile();
    moving = false;
    undone = false;
    if(isGameWon == true)
        gameWon();
}

void Board::clearTiles(Tile *tilesToClear[4][4])
{
    for(int i=0; i<4; i++)
    {
        for(int j=0; j<4; j++)
        {
            if(tilesToClear[i][j] != nullptr)
                tilesToClear[i][j]->deleteLater();
            tilesToClear[i][j] = nullptr;
        }
    }
}

void Board::paintTiles(Tile *tilesToPaint[4][4], int numbers[4][4])
{
    for(int i=0; i<4; i++)
    {
        for(int j=0; j<4; j++)
        {
            if(tilesToPaint[i][j] != nullptr)
            {
                tilesToPaint[i][j] = new Tile(numbers[i][j], this);
                tilesToPaint[i][j]->setPos(this->boundingRect().x() + this->boundingRect().width()/4*i, this->boundingRect().y() + this->boundingRect().height()/4*j);
            }
        }
    }
}

void Board::undo()
{
    if(undone == true || moving == true)
        return;
    undone = true;
    this->clearTiles(this->tiles);
    for(int i=0; i<4; i++)
    {
        for(int j=0; j<4; j++)
        {
            this->tiles[i][j] = this->oldTiles[i][j];
        }
    }
    this->paintTiles(this->tiles, this->oldTilesNumber);
    score = oldScore;
    this->scoreChanged(QString::number(score));
}
