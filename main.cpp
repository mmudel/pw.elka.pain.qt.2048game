#include "gamewindow.h"
#include <QApplication>
#include <QTime>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
    GameWindow w;
    w.show();
    w.fitInView();

    return a.exec();
}
