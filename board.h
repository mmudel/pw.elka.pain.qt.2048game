#ifndef BOARD_H
#define BOARD_H

#include <QObject>
#include <QGraphicsItem>
#include "tile.h"
#include "point.h"
#include "constants.h"

class Board : public QObject, public QGraphicsItem
{
    Q_OBJECT
public:
    explicit Board(QObject *parent = 0);
    Tile* tiles[4][4];
    /* table, telling how board should look like when move will end */
    Tile* newTiles[4][4];
    /* table, telling how board look like before last move */
    Tile* oldTiles[4][4];
    /* table, telling which numbers has tiles before last move */
    int oldTilesNumber[4][4];
    /* function creating new tile */
    void createTile();
    /* function joining tiles */
    void joinTiles(Point from, Point to);
    /* function run when user press 'W', 'S', 'A' or 'D' to move tiles */
    void moveTiles(int key);
    /* function run when all tiles stop moving */
    void endMove();
    /* function run to undo last move */
    void undo();

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;
    QRectF boundingRect() const Q_DECL_OVERRIDE;
protected:
    void advance(int phase) Q_DECL_OVERRIDE;
signals:
    void scoreChanged(const QString &);
    void gameWon();
public slots:
private:
    void move(int iStart, int iEnd, int jStart, int jEnd);
    void clearTiles(Tile* tiles[4][4]);
    void paintTiles(Tile* tiles[4][4], int numbers[4][4]);
    /* number telling how many points player has */
    int score;
    /* number telling how many points player had before last move */
    int oldScore;
    /* bool setted to prevent undo while one undo was done */
    bool undone = true;
    /* bool setted to prevent action while moving */
    bool moving = false;
    /* bool telling if we reached 2048 */
    bool isGameWon = false;

};

#endif // BOARD_H
