#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include <QMainWindow>
#include <QGraphicsItem>
#include "board.h"

namespace Ui {
class GameWindow;
}

class GameWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit GameWindow(QWidget *parent = 0);
    void fitInView();
    ~GameWindow();
    void setScore();

protected:
    void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;

private slots:
    void on_undoButton_clicked();
    void gameWon();

private:
    Ui::GameWindow *ui;
    Board* board;
    QGraphicsScene* scene;
    void resizeEvent(QResizeEvent * event);
    void newGame();
};

#endif // GAMEWINDOW_H
