#ifndef TILE_H
#define TILE_H

#include <QObject>
#include <QGraphicsItem>
#include <QPainter>
#include <constants.h>

class Tile : public QObject, public QGraphicsItem
{
    Q_OBJECT
public:
    explicit Tile(int number, QGraphicsItem *parent = 0);
    Qt::GlobalColor color;
    float moveX = 0;
    float moveY = 0;
    bool destroy = false;
    float moveDiff;
    int move();
    int increaseNumber();
    int getNumber();
protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;
    QRectF boundingRect() const Q_DECL_OVERRIDE;
signals:
public slots:
private:
    void mapColor();
    int direction;
    int number;
};

#endif // TILE_H
